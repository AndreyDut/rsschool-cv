const projects = [
    {
    image: 'img/Weather.png',
    title: 'Fancy-weather', 
    discription_1: 'App-weather forecast.',
    discription_2: 'When you log in, it shows your location.',
    discription_3: 'You can change the language, temperature units, and background image.',
    discription_4: 'Search by city and country with weather indication.',
    technoligies: 'Webpack and API were used.',
    link: 'https://andreydut.gitlab.io/rsschool-cv/page-weather/'
    },
    {
        image: 'img/SINGOLO.png',
        title: 'Simple-singolo', 
        discription_1: 'Single-page website.',
        discription_2: 'A slider is implemented on the page.',
        discription_3: 'Images in the portfolio change their position when switching tabs.',
        discription_4: 'Navigating through menu anchors.',
        technoligies: 'Only HTML/CSS , JS were used.',
        link: 'https://andreydut.gitlab.io/rsschool-cv/singolo/'
        },
    

]
const toHtml = project => `<div class="wrapper-project is-clickable">
<img class="img-project" src="${project.image}" alt="">
<div class="description-project">
    <h3 class="description-project__name">${project.title}</h3>
    <p class="description-project__text">${project.discription_1}</p>
    <p class="description-project__text">${project.discription_2}</p>
    <p class="description-project__text">${project.discription_3}</p>
    <p class="description-project__text">${project.discription_4}</p>
    <p class="description-project__text"></p>
</div>
<a target="_blank" href="${project.link}"></a>
</div>`;

function createDiv(){
    const div =document.createElement('div');
    const html = projects.map(project => toHtml(project)).join('');
    div.innerHTML = html
    document.querySelector('.my-projects').append(div);
}
createDiv()
