# Resume

*1. First Name, Last Name:*  
-
  * **Andrey Dutkovski**


*2. Contact Info:*  
- 
  * **Outlook:** Andrey_Dutkouski@epam.com
  * **Skype:** Andrey Dutkouski
  * **Telegram:** @AndreyDut
  
*3. Summary:*
-
My goal is to develop in my chosen direction and achieve the desired result. It is necessary to constantly engage in self-development and learn to find a solution to the task.

*4. Skills*
-
  * **HTML Basic**
  * **CSS Basic**
  * **JavaScript Basic**

*5. Code examples:* -
-
*6. Experience:*
-
  * **Tests when studying HTML/CSS basic and JS basic.**
  * **Solving Codewars.com Tests.**

*7. Education:*
-
  * **Military Academy of the Republic of Belarus 2014-2019**
  * **EPAM UpSkill Lab 2020 - to the present time**

*8. English:*
-
  * **A1**