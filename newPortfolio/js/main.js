const projects = [
  {
    id: "weather",
    desktop: "url(img/projects/weather/Weather.png)",
    tablet: "url(img/projects/weather/Weather-tablet.png)",
    smartphone: "url(img/projects/weather/Weather-mobile.png)",
    title: "Fancy-weather",
    discription_1: "App-weather forecast.",
    discription_2: "When you log in, it shows your location.",
    discription_3:
      "You can change the language, temperature units, and background image.",
    technoligies: "Webpack and API were used.",
    linkDemo: "https://andreydut.gitlab.io/rsschool-cv/page-weather/",
    linkGit:
      "https://github.com/AndreyDut/rsschool-cv/tree/page-weather/page-weather",
  },
  {
    id: "singolo",
    desktop: "url(img/projects/singolo/SINGOLO.png)",
    tablet: "url(img/projects/singolo/SINGOLO-tablet.png)",
    smartphone: "url(img/projects/singolo/SINGOLO-mobile.png)",
    title: "Simple-singolo",
    discription_1: "Single-page website.",
    discription_2: "A slider is implemented on the page.",
    discription_3:
      "Images in the portfolio change their position when switching tabs.",
    technoligies: "Only HTML/CSS , JS were used.",
    linkDemo: "https://andreydut.gitlab.io/rsschool-cv/singolo/",
    linkGit: "https://github.com/AndreyDut/rsschool-cv/tree/singolo/singolo",
  },
  {
    id: "project-name",
    desktop: "url(img/projects/project.jpg)",
    tablet: "url(img/projects/project.jpg)",
    smartphone: "url(img/projects/project.jpg)",
    title: "Name project",
    discription_1: "project description",
    discription_2: "project description",
    discription_3: "project description",
    technoligies: "project technologies",
    linkDemo:
      "http://тейково-район.рф/tinybrowser/images/stranitsa-v-razrabotke.jpg",
    linkGit:
      "http://тейково-район.рф/tinybrowser/images/stranitsa-v-razrabotke.jpg",
  },
];
document.querySelectorAll(".portfolio-project__btn-show").forEach((elem) => {
  elem.addEventListener("click", (e) => {
    let div = document.createElement("div");
    let html = toHTML(projects.find((project) => project.id == elem.id));
    div.classList.add("open-project");
    div.innerHTML = html;
    document.querySelector(".wrapper").append(div);
    $(".slider").slick();
    (function ($) {
      $(".is-clickable").on("click", function (evt) {
        if (!$(evt.target).is("a")) {
          isClickable($(this));
        }
      });
    })(jQuery);
    document.querySelector(".open-project").classList.add("window-project");
    document.querySelector(".fa-times").addEventListener("click", () => {
      document.querySelector(".open-project").classList.add("close-project");
      document
        .querySelector(".close-project")
        .addEventListener("transitionend", () => {
          document.querySelector(".open-project").remove();
        });
    });
  });
});

function toHTML(project) {
  return `<i class="fas fa-times fa-2x"></i>
    <div class="slider">
    <div style="background-image: ${project.desktop};" class="size-background"></div>
    <div style="background-image: ${project.tablet};" class="size-background"></div>
    <div style="background-image: ${project.smartphone};" class="size-background"></div>
    </div>
    <div class="description-project">
      <h2 class="description-project__name">${project.title}</h2>
      <p class="description-project__text">${project.discription_1}</p>
      <p class="description-project__text">${project.discription_2}</p>
      <p class="description-project__text">${project.discription_3}</p>
      <p class="description-project__text">${project.technoligies}</p>
      <div class="links">
          <button  type="button" class="links__btn is-clickable">DEMO<a target="_blank" href="${project.linkDemo}"></a></button>
          <button type="button" class="links__btn is-clickable">GITHUB<a target="_blank" href="${project.linkGit}"></a></button>
      </div>
  </div>`;
}
(function ($) {
  $(".contacts-row__link").on("click", function (evt) {
    if (!$(evt.target).is("a")) {
      isClickable($(this));
    }
  });
})(jQuery);
(function ($) {
  $(".about-me-info__btn-download").on("click", function (evt) {
    if (!$(evt.target).is("a")) {
      isClickable($(this));
    }
  });
})(jQuery);

function isClickable(obj, newTab) {
  let $this = obj,
    link = $this.find("a:first"),
    href = link.attr("href"),
    target = link.attr("target");

  if (href == undefined) {
    return;
  }
  if (target == "_blank" || newTab) {
    window.open(href);
  } else {
    window.location.href = href;
  }
}
document.querySelectorAll(".portfolio-project").forEach((elem) => {
  elem.addEventListener("touchstart", () => {
    document.querySelector(`#${elem.id} .portfolio-project__img`).classList.toggle("touch-img");
    document
      .querySelector(`#${elem.id} .portfolio-project__overlay`)
      .classList.toggle("touch-overlay");
    document
      .querySelector(`#${elem.id} h2`)
      .classList.toggle("touch-h2-button");
    document
      .querySelector(`#${elem.id} .portfolio-project__btn-show`)
      .classList.toggle("touch-h2-button");
  });
});

document.querySelector(".fa-bars").addEventListener("click", () => {
  document.querySelector(".header").classList.toggle("on");
  document.querySelectorAll(".header-main-menu__elem").forEach((elem) => {
    elem.addEventListener("click", () =>
      document.querySelector(".header").classList.remove("on")
    );
  });
});
window.addEventListener(
  "load",
  () => (document.querySelector(".page-loading").remove())
);
